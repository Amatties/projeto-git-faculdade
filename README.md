# The Project

This project has as main objective just to make a good use of the tools in the Git, 
making an organised and well done developing of the code and the project itself

It is made from a **diagram** about a school with user who are or teachers or students, disciplines and college courses.

# Purpose

It is a Java project made for students of the **Faculdade de Tecnologia SENAC/RS**, in Brazil, using a Mysql database.

# Used feeder

We used a framework called ***Primefaces*** to insert the information on the database.
You can have more information about Primefaces **[here](http://187.7.106.14/angelo/Algoritmos%20e%20Programacao%20III/Aula%2010/primefaces_user_guide_5_2.pdf "More information")**.

# How to make?

Not much is needed to run this project, just download the project, create a **Mysql** database connection and start it.

To create the ER diagram was used the Mysql Workbench (more information about it can be find in **[here](https://www.mysql.com/products/workbench/ "More information")**) 
and to create the Class diagram was used the Astah **[click here for more information](http://astah.net/editions/community "More information")**.
To create the connection with the database, Xampp was used. You can have more information of how to use Xampp **[clicking here](https://blog.udemy.com/xampp-tutorial/ "More information")**.

**It is just a college project, made by students who are just starting to use git and trying to learn something new, feel free to use the code as you wish!**

## Previous Diagram

![Previous Diagram](https://image.ibb.co/dokjQ5/2017_03_14_Aula_02_ER_Exercicio_05.jpg)

## Class Diagram

### Model Class Diagram



![Model Class Diagram](http://image.ibb.co/k7Wpf5/Class_Diagram.png)  


### Full Class Diagram

![Full Class Diagram](https://image.ibb.co/cO8qF5/Classes.jpg)  


## For more information access

* **Primefaces:** <https://www.primefaces.org/>
* **Wikipedia:** <https://en.wikipedia.org/wiki/Java_Platform,_Enterprise_Edition>