package faculdade.controller;

import faculdade.dao.Disciplina;
import faculdade.controller.util.JsfUtil;
import faculdade.controller.util.JsfUtil.PersistAction;
import faculdade.service.DisciplinaFacade;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("disciplinaController")
@SessionScoped
public class DisciplinaController implements Serializable {

    @EJB
    private faculdade.service.DisciplinaFacade ejbFacade;
    private List<Disciplina> items = null;
    private Disciplina selected;

    public DisciplinaController() {
    }

    public Disciplina getSelected() {
        return selected;
    }

    public void setSelected(Disciplina selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getDisciplinaPK().setCursoid(selected.getCurso().getId());
    }

    protected void initializeEmbeddableKey() {
        selected.setDisciplinaPK(new faculdade.dao.DisciplinaPK());
    }

    private DisciplinaFacade getFacade() {
        return ejbFacade;
    }

    public Disciplina prepareCreate() {
        selected = new Disciplina();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("DisciplinaCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("DisciplinaUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("DisciplinaDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Disciplina> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    /**
     * Executa quaisquer ações de modificação de dados para uma entidade. As
     * ações que Podem ser realizados por este método são controlados pelo
     * Enumeração {@link PersistAction} e são CREATE, EDIT ou DELETE.
     *
     * @param persistAction uma ação específica que deve ser executada no Item
     * atual
     * @param successMessage uma mensagem que deve ser exibida quando
     * persistindo O item é bem-sucedido
     */
    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Disciplina getDisciplina(faculdade.dao.DisciplinaPK id) {
        return getFacade().find(id);
    }

    public List<Disciplina> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Disciplina> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Disciplina.class)
    public static class DisciplinaControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            DisciplinaController controller = (DisciplinaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "disciplinaController");
            return controller.getDisciplina(getKey(value));
        }

        faculdade.dao.DisciplinaPK getKey(String value) {
            faculdade.dao.DisciplinaPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new faculdade.dao.DisciplinaPK();
            key.setId(Integer.parseInt(values[0]));
            key.setCursoid(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(faculdade.dao.DisciplinaPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getId());
            sb.append(SEPARATOR);
            sb.append(value.getCursoid());
            return sb.toString();
        }

        /**
         *
         * @param facesContext
         * @param component
         * @param object
         * @return é um metodo que retorna um objeto verdadeiro
         */
        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Disciplina) {
                Disciplina o = (Disciplina) object;
                return getStringKey(o.getDisciplinaPK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Disciplina.class.getName()});
                return null;
            }
        }

    }

}
