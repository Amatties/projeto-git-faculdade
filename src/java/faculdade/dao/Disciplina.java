package faculdade.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Consultas feitas pela engenharia reversa do primefaces.
 */
//<editor-fold defaultstate="collapsed" desc="Queries">
@Entity
@Table(name = "disciplina")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Disciplina.findAll", query = "SELECT d FROM Disciplina d")
    , @NamedQuery(name = "Disciplina.findById", query = "SELECT d FROM Disciplina d WHERE d.disciplinaPK.id = :id")
    , @NamedQuery(name = "Disciplina.findByNome", query = "SELECT d FROM Disciplina d WHERE d.nome = :nome")
    , @NamedQuery(name = "Disciplina.findByCargahoraria", query = "SELECT d FROM Disciplina d WHERE d.cargahoraria = :cargahoraria")
    , @NamedQuery(name = "Disciplina.findByCursoid", query = "SELECT d FROM Disciplina d WHERE d.disciplinaPK.cursoid = :cursoid")})
//</editor-fold>
public class Disciplina implements Serializable {

    /**
     * Declaração de atributos feita pela engenharia reversa do primefaces.
     */
    //<editor-fold defaultstate="collapsed" desc="Atributos">
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DisciplinaPK disciplinaPK;
    @Size(max = 45)
    @Column(name = "nome")
    private String nome;
    @Column(name = "cargahoraria")
    @Temporal(TemporalType.TIME)
    private Date cargahoraria;
    @JoinColumn(name = "Curso_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Curso curso;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "disciplina")
    private Collection<Usuario> usuarioCollection;
// </editor-fold>

    public Disciplina() {
    }

// <editor-fold defaultstate="collapsed" desc="Getters e Setters">
    public Disciplina(DisciplinaPK disciplinaPK) {
        this.disciplinaPK = disciplinaPK;
    }

    public Disciplina(int id, int cursoid) {
        this.disciplinaPK = new DisciplinaPK(id, cursoid);
    }

    public DisciplinaPK getDisciplinaPK() {
        return disciplinaPK;
    }

    public void setDisciplinaPK(DisciplinaPK disciplinaPK) {
        this.disciplinaPK = disciplinaPK;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getCargahoraria() {
        return cargahoraria;
    }

    public void setCargahoraria(Date cargahoraria) {
        this.cargahoraria = cargahoraria;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }// </editor-fold>

    @XmlTransient
    public Collection<Usuario> getUsuarioCollection() {
        return usuarioCollection;
    }

    public void setUsuarioCollection(Collection<Usuario> usuarioCollection) {
        this.usuarioCollection = usuarioCollection;
    }
// <editor-fold defaultstate="collapsed" desc="Métodos Override">

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disciplinaPK != null ? disciplinaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Disciplina)) {
            return false;
        }
        Disciplina other = (Disciplina) object;
        if ((this.disciplinaPK == null && other.disciplinaPK != null) || (this.disciplinaPK != null && !this.disciplinaPK.equals(other.disciplinaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return " " + curso.getNome() + " / " + nome + " ";
    }//</editor-fold>

}
