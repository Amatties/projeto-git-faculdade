package faculdade.dao;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Alberto
 */
@Embeddable
public class DisciplinaPK implements Serializable {

    /**
     * Atributos criados pela engenharia reversa do PF.
     */
    //<editor-fold defaultstate="collapsed" desc="Atributos.">
    @Basic(optional = false)
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Curso_id")
    private int cursoid;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Construtores.">
    public DisciplinaPK() {
    }

    public DisciplinaPK(int id, int cursoid) {
        this.id = id;
        this.cursoid = cursoid;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Getters & Setters.">

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCursoid() {
        return cursoid;
    }

    public void setCursoid(int cursoid) {
        this.cursoid = cursoid;
    }
//</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Métodos Override.">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) cursoid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DisciplinaPK)) {
            return false;
        }
        DisciplinaPK other = (DisciplinaPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.cursoid != other.cursoid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "faculdade.dao.DisciplinaPK[ id=" + id + ", cursoid=" + cursoid + " ]";
    }
    //</editor-fold>
}
