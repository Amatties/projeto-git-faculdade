package faculdade.dao;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Consultas feitas pela engenharia reversa do primefaces.
 */
//<editor-fold defaultstate="collapsed" desc="Queries">
@Entity
@Table(name = "usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findById", query = "SELECT u FROM Usuario u WHERE u.usuarioPK.id = :id")
    , @NamedQuery(name = "Usuario.findByNome", query = "SELECT u FROM Usuario u WHERE u.nome = :nome")
    , @NamedQuery(name = "Usuario.findBySenha", query = "SELECT u FROM Usuario u WHERE u.senha = :senha")
    , @NamedQuery(name = "Usuario.findByIdade", query = "SELECT u FROM Usuario u WHERE u.idade = :idade")
    , @NamedQuery(name = "Usuario.findBySalario", query = "SELECT u FROM Usuario u WHERE u.salario = :salario")
    , @NamedQuery(name = "Usuario.findByMatricula", query = "SELECT u FROM Usuario u WHERE u.matricula = :matricula")
    , @NamedQuery(name = "Usuario.findByDisciplinaid", query = "SELECT u FROM Usuario u WHERE u.usuarioPK.disciplinaid = :disciplinaid")
    , @NamedQuery(name = "Usuario.findByDisciplinaCursoid", query = "SELECT u FROM Usuario u WHERE u.usuarioPK.disciplinaCursoid = :disciplinaCursoid")})
//</editor-fold>
public class Usuario implements Serializable {

    /**
     * Declaração de atributos feita pela engenharia reversa do primefaces.
     */
    //<editor-fold defaultstate="collapsed" desc="Atributos">
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UsuarioPK usuarioPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "senha")
    private String senha;
    @Column(name = "idade")
    private Integer idade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "salario")
    private Double salario;
    @Column(name = "matricula")
    private Integer matricula;
    //</editor-fold>

    /**
     * Join para a identificação da disciplina no curso do usuário.
     */
    @JoinColumns({
        @JoinColumn(name = "Disciplina_id", referencedColumnName = "id", insertable = false, updatable = false)
        , @JoinColumn(name = "Disciplina_Curso_id", referencedColumnName = "Curso_id", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Disciplina disciplina;

    /**
     * Construtores Sobrecarregados.
     */
    public Usuario() {
    }

    public Usuario(UsuarioPK usuarioPK) {
        this.usuarioPK = usuarioPK;
    }

    public Usuario(UsuarioPK usuarioPK, String nome, String senha) {
        this.usuarioPK = usuarioPK;
        this.nome = nome;
        this.senha = senha;
    }

    public Usuario(int id, int disciplinaid, int disciplinaCursoid) {
        this.usuarioPK = new UsuarioPK(id, disciplinaid, disciplinaCursoid);
    }
// <editor-fold defaultstate="collapsed" desc="Getters e Setters">

    public UsuarioPK getUsuarioPK() {
        return usuarioPK;
    }

    public void setUsuarioPK(UsuarioPK usuarioPK) {
        this.usuarioPK = usuarioPK;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Integer getMatricula() {
        return matricula;
    }

    public void setMatricula(Integer matricula) {
        this.matricula = matricula;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    //</editor-fold>
// <editor-fold defaultstate="collapsed" desc="Métodos Override">
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuarioPK != null ? usuarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usuarioPK == null && other.usuarioPK != null) || (this.usuarioPK != null && !this.usuarioPK.equals(other.usuarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return " " + nome + " ";
    }//</editor-fold>

}
