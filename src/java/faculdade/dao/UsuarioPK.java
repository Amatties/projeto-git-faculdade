package faculdade.dao;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Alberto
 */
@Embeddable
public class UsuarioPK implements Serializable {

    /**
     * Atributos criados pela engenharia reversa do PF.
     */
    //<editor-fold defaultstate="collapsed" desc="Atributos.">
    @Basic(optional = false)
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Disciplina_id")
    private int disciplinaid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Disciplina_Curso_id")
    private int disciplinaCursoid;

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Construtores.">
    public UsuarioPK() {
    }

    public UsuarioPK(int id, int disciplinaid, int disciplinaCursoid) {
        this.id = id;
        this.disciplinaid = disciplinaid;
        this.disciplinaCursoid = disciplinaCursoid;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Getters & Setters.">

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDisciplinaid() {
        return disciplinaid;
    }

    public void setDisciplinaid(int disciplinaid) {
        this.disciplinaid = disciplinaid;
    }

    public int getDisciplinaCursoid() {
        return disciplinaCursoid;
    }

    public void setDisciplinaCursoid(int disciplinaCursoid) {
        this.disciplinaCursoid = disciplinaCursoid;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Métodos Override.">

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) disciplinaid;
        hash += (int) disciplinaCursoid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioPK)) {
            return false;
        }
        UsuarioPK other = (UsuarioPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.disciplinaid != other.disciplinaid) {
            return false;
        }
        if (this.disciplinaCursoid != other.disciplinaCursoid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "faculdade.dao.UsuarioPK[ id=" + id + ", disciplinaid=" + disciplinaid + ", disciplinaCursoid=" + disciplinaCursoid + " ]";
    }
    //</editor-fold>

}
