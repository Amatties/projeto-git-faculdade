package faculdade.service;

import faculdade.dao.Disciplina;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Alberto
 */
@Stateless
public class DisciplinaFacade extends AbstractFacade<Disciplina> {

    @PersistenceContext(unitName = "FaculdadePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DisciplinaFacade() {
        super(Disciplina.class);
    }

}
